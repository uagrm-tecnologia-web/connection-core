/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectioncore;

import communication.MailVerificationThread;
import communication.SendEmailThread;
import events.ReceiveEmailEvent;
import interfaces.IEmailEventListener;
import java.util.List;
import utils.Email;

/**
 *
 * @author ronal
 */
public class ConnectionCore {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MailVerificationThread mailVerificationThread = new MailVerificationThread();
        mailVerificationThread.setEmailEventListener(new IEmailEventListener() {
            @Override
            public void onReceiveEmailEvent(ReceiveEmailEvent event) {
                List<Email> emails = event.getEmails();
                for (Email email : emails) {
                    System.out.println(email);
                }
            }
        });
        Thread thread = new Thread(mailVerificationThread);
        thread.setName("Mail Verfication Thread");
        thread.start();
        
        
        
        /*
        Email emailObject = new Email("ronaldorivero3@gmail.com", Email.SUBJECT,
                "Petición realizada correctamente");
        
        SendEmailThread sendEmail = new SendEmailThread(emailObject);
        Thread thread = new Thread(sendEmail);
        thread.setName("Send email Thread");
        thread.start();*/
    }
    
}
