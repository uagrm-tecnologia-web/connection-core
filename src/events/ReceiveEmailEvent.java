/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import java.util.EventObject;
import java.util.List;
import utils.Email;

/**
 *
 * @author ronal
 */
public class ReceiveEmailEvent extends EventObject{
    
    private List<Email> emails;

    public ReceiveEmailEvent(Object source, List<Email> emails) {
        super(source);
        this.emails = emails;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }
}
