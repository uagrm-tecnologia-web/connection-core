/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import common.ConfReader;
import events.ReceiveEmailEvent;
import interfaces.IEmailEventListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.sasl.AuthenticationException;

import utils.Command;
import utils.Email;
import utils.Extractor;

/**
 *
 * @author ronal
 */
public class MailVerificationThread implements Runnable{
    private Socket socket;
    private BufferedReader input;
    private DataOutputStream output;
    
    private ConfReader reader;
    private IEmailEventListener emailEventListener;
    
    public MailVerificationThread() {
        socket = null;
        input = null;
        output = null;
        reader = new ConfReader();
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<Email> emails = null;
                socket = new Socket(reader.get("mail_host"), Integer.parseInt(reader.get("mail_pop_port")));
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                output = new DataOutputStream(socket.getOutputStream());
                System.out.println("**********Connection open**********");
                
                authUser(reader.get("mail_user"), reader.get("mail_pass"));
                
                output.writeBytes(Command.stat());
                
                int count_emails = getEmailCount(input.readLine());
                if(count_emails > 0){
                    
                    emails = getEmails(count_emails);
                    
                    System.out.println(emails);
                    
                    deleteEmails(count_emails);
                }
                output.writeBytes(Command.quit());
                input.readLine();
                input.close();
                output.close();
                socket.close();
                System.out.println("**********Connection close*********");
                if(count_emails > 0){
                    triggerEmailEventListener(emails);
                }
                Thread.sleep(2000);
            } catch (AuthenticationException ae) {
                System.err.println("MailVerificationThread.run: Authentication failed");
            } catch (IOException ex) {
                Logger.getLogger(MailVerificationThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(MailVerificationThread.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }

    public IEmailEventListener getEmailEventListener() {
        return emailEventListener;
    }

    public void setEmailEventListener(IEmailEventListener emailEventListener) {
        this.emailEventListener = emailEventListener;
    }
    
    private void authUser(String user, String password) throws IOException {
        if (socket != null && input != null && output != null) {
            input.readLine();
            output.writeBytes(Command.user(user));
            input.readLine();
            output.writeBytes(Command.pass(password));
            String message = input.readLine();
            if (message.contains("-ERR")) {
                throw new AuthenticationException();
            }
        }
    }
    
    private void deleteEmails(int emails) throws IOException{
        for(int i = 1; i <= emails; i++){
            output.writeBytes(Command.dele(i));
        }
    }
    
    private String readMultiline() throws IOException {
        String lines = "";
        while (true) {
            String line = input.readLine();
            if (line == null) {
                // Server closed connection
                throw new IOException("S : Server unawares closed the connection.");
            }
            if (line.equals(".")) {
                break;
            }
            lines = lines + "\n" + line;
        }
        return lines;
    }
    
    private void triggerEmailEventListener(List<Email> emails){
        if(emailEventListener != null){
            ReceiveEmailEvent event = new ReceiveEmailEvent(this, emails);
            emailEventListener.onReceiveEmailEvent(event);
        }
    }
    
    private int getEmailCount(String line){
        String[] data = line.split(" ");
        return Integer.parseInt(data[1]);
    }

    private List<Email> getEmails(int count_emails) throws IOException {
        List<Email> emails = new ArrayList<>();
        for(int i = 1; i <= count_emails; i++){
            output.writeBytes(Command.retr(i));
            
            String plain_text = readMultiline();
            
            emails.add(Extractor.getEmail(plain_text));
        }
        return emails;
    }
}
